package controllers

import (
	"bitbucket.org/encryptoserver/models"
	"github.com/astaxie/beego"
	"encoding/json"
	"time"
	"fmt"
)

type FilesController struct {
	beego.Controller
}

// List returns all the files that exist in s3 that the user can access
//Files should have an expiry date (or a download count)
func (this *FilesController) List() {
    if p, ok := this.Ctx.Input.GetData("profile").(models.GoogleProfile); ok {
    	fmt.Printf("profile: %+v\n", p)
    	if files, err := retrieveFilesForProfile(p.Email); err != nil {
    		this.Data["json"] = err
		} else {
			this.Data["json"] = files
		}

    } else {
    	this.Data["json"] = nil
    }

	this.ServeJSON()
}

// List returns all the files that exist in s3 that the user can access
//Files should have an expiry date (or a download count)
func (this *FilesController) ListAll() {
    if p, ok := this.Ctx.Input.GetData("profile").(models.GoogleProfile); ok {
    	fmt.Printf("profile: %+v\n", p)
    	if files, err := retrieveAllFiles(); err != nil {
    		this.Data["json"] = err
		} else {
			this.Data["json"] = files
		}

    } else {
    	this.Data["json"] = nil
    }

	this.ServeJSON()
}

//Retrieve allows a user to download specific files
func (this *FilesController) Retrieve() {

	filename := this.GetString("filename") //encrypted file name
	fmt.Println("filename: " + filename)

    if p, ok := this.Ctx.Input.GetData("profile").(models.GoogleProfile); ok {
    	fmt.Printf("profile: %+v\n", p)
    	if file, err := retrieveFileForProfile(p.Email, filename); err != nil {
    		this.Ctx.Output.SetStatus(500)
    		this.Data["json"] = err
		} else {
			if err, file.Content = downloadFromS3(filename); err != nil {
				this.Ctx.Output.SetStatus(500)
				this.Data["json"] = err
			} else {
				this.Data["json"] = file
			}
		}
    } else {
    	fmt.Println("no session")
    	this.Data["json"] = nil
    }
	this.ServeJSON()
}

//Upload is how the client can upload files to storage
func (this *FilesController) Upload() {
	var weeksToExpiry int
	weeksToExpiry = 1
	var f models.File
	json.Unmarshal(this.Ctx.Input.RequestBody, &f)
	f.Expiry = time.Now()
	f.Expiry.AddDate(0, 0, 7 * weeksToExpiry)
	f.FileSize = len(f.Content)
	profile := this.Ctx.Input.GetData("profile").(models.GoogleProfile)
	f.Sender = profile.Email //set the sender of the file
	// fmt.Printf("file: %+v\n", f)
	if err, ok := addFileForProfile(f); err != nil {
		fmt.Println("error adding file for profile: " + err.Error())
		this.Data["json"] = err
	} else if err := uploadToS3(ok, f.Name, &(f.Content)); err != nil {
		fmt.Println("error uploading file to s3: " + err.Error())
		this.Data["json"] = err
	} else {
		fmt.Println("no error")
		this.Data["json"] = nil
	}
	this.ServeJSON()
}

package controllers

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"bitbucket.org/encryptoserver/models"
	"github.com/jinzhu/gorm"
	"github.com/thanhpk/randstr"
	"os"
	"fmt"
	"errors"
	"reflect"
	// "time"
)

var db *gorm.DB

func InitDB() {
	username := os.Getenv("DATABASE_USERNAME")
	password := os.Getenv("DATABASE_PASSWORD")
	host := os.Getenv("DATABASE_HOST")
	dbname := "encryptionserver" //os.Getenv("DATABASE_NAME")

	if username == "" {
		fmt.Println("ENV VARS missing")
		os.Exit(1)
	}
	//configure the DB
	var err error
  db, err = gorm.Open("postgres", "host="+host+" user="+username+" dbname="+dbname+" sslmode=disable password="+password)
  if err != nil {
    panic("failed to connect database: " + err.Error())
  }
  // defer db.Close()
  db.AutoMigrate(&models.GoogleProfile{}, &models.File{}, &models.Key{})
}
func createUser(user *models.GoogleProfile) (error) {
	user.ApiKey = randstr.String(16)
	err := db.Create(user).Error
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func addFileForProfile(file models.File) (error, bool) {

	if f, err := retrieveFileForProfile(file.UserID, file.Name); err != nil {
		// return err, true //didn't find it so upload
		fmt.Printf("error retrieving: %s\n", err.Error())
	} else if !reflect.DeepEqual(f, models.File{}) {
		//there was a file
		return nil, false //don't upload
	}
	err := db.Create(&file).Error
	if err != nil {
		fmt.Println(err)
		return err, false //don't upload
	}
	return nil, true //file stored and ready for upload
}
func retrieveFileForProfile(email, filename string) (models.File, error) {
	var f models.File
	fmt.Println("looking for: " + email)
	db.Where("user_id = ? and name = ?", email, filename).First(&f)

	if reflect.DeepEqual(f, models.File{}) {
		return models.File{}, errors.New("we didn't find a file")
	}
	return f, nil
}
func retrieveFilesForProfile(email string) ([]models.File, error) {
	var f []models.File
	fmt.Println("looking for: " + email)
	db.Where("user_id = ?", email).Find(&f)

	fmt.Println("result: ", f)
	if reflect.DeepEqual(f, []models.File{}) {
		return []models.File{}, errors.New("we didn't find any files")
	}
	return f, nil
}
func deleteFilesForProfile(email string) (error) {
	//no questions asked
	fmt.Println("deleting files for: " + email)
	db.Where("user_id = ?", email).Delete(models.File{})
	return nil
}
func retrieveAllFiles() ([]models.File, error) {
	var f []models.File
	db.Find(&f)

	fmt.Println("result: ", f)
	if reflect.DeepEqual(f, []models.File{}) {
		return []models.File{}, errors.New("we didn't find any files")
	}
	return f, nil
}
func addKeyForUser(email string, content string) (error, bool) {
	var key models.Key
	key.Content = content
	key.UserID = email
	if k, err := retrieveKeyForUser(email); err != nil {
		fmt.Printf("error retrieving: %s\n", err.Error())
	} else if !reflect.DeepEqual(k, models.Key{}) {
		fmt.Printf("not empty: %+v", k)
		//there was a key
		return nil, false //don't upload
	}
	err := db.Create(&key).Error
	if err != nil {
		fmt.Println("error uploading key: " + err.Error())
		return err, false //don't upload
	}
	return nil, true //file stored and ready for upload
}
// WARNINGthis overrides any key found regardless.
func modifyKeyForUser(email string, content string) (error, bool) {
	var key models.Key
	key.Content = content
	key.UserID = email
	err := db.Create(&key).Error
	if err != nil {
		fmt.Println("error uploading key: " + err.Error())
		return err, false //don't upload
	}
	return nil, true //file stored and ready for upload
}
func retrieveKeyForUser(email string) (models.Key, error) {
	var k models.Key
	db.Where("user_id = ?", email).First(&k)

	fmt.Println("result: ", k)
	if reflect.DeepEqual(k, models.Key{}) {
		return models.Key{}, errors.New("we didn't find any keys")
	}
	return k, nil
}
func retrieveKeys() ([]models.Key, error) {
	var k []models.Key
	db.Find(&k)

	fmt.Println("result: ", k)
	if reflect.DeepEqual(k, []models.Key{}) {
		return []models.Key{}, errors.New("we didn't find any keys")
	}
	return k, nil
}
func retrieveKeysLike(match string) ([]models.Key, error) {
	var k []models.Key
	db.Where("user_id LIKE ?", "%"+match+"%").Find(&k)

	fmt.Println("result: ", k)
	if reflect.DeepEqual(k, models.Key{}) {
		return []models.Key{}, errors.New("we didn't find any keys")
	}
	return k, nil
}

func retrieveUserProfile(emailAddress string) (models.GoogleProfile, error) {
	var p models.GoogleProfile
	fmt.Println("looking for: " + emailAddress)
	db.Where("email = ?", emailAddress).First(&p)

	fmt.Println("result: ", p)
	if reflect.DeepEqual(p, models.GoogleProfile{}) {
		return models.GoogleProfile{}, errors.New("we didn't find a user")
	}
	return p, nil
}
func retrieveUserProfileById(id string) (models.GoogleProfile, error) {
	var p models.GoogleProfile
	fmt.Printf("looking for: %s\n", id)
	db.Where("id = ?", id).First(&p)

	fmt.Println("result: ", p)
	if reflect.DeepEqual(p, models.GoogleProfile{}) {
		return models.GoogleProfile{}, errors.New("we didn't find a user")
	}
	return p, nil
}
package controllers

import (
	"github.com/astaxie/beego/context"
	"bitbucket.org/encryptoserver/models"
  "net/http"
	"fmt"
)

func AllMiddleWare(ctx *context.Context) {
	if u, ok := ctx.Input.Session("user").(models.GoogleProfile); ok {
    	fmt.Println("image url: ", u.Picture)
		ctx.Input.SetData("Picture", u.Picture)
		fmt.Println("email: ", u.Email)
		ctx.Input.SetData("User", u.Email)
    }
}
func AuthMiddleWare(ctx *context.Context) {
    fmt.Println("token: " + ctx.Input.Query("id_token"))
   	if token := ctx.Input.Query("id_token"); token == "" {
        ctx.WriteString("error. There is no token.")
   	} else {
   		if response, err := RequestUserFromGoogle(token); err != nil {
        fmt.Println("token invalid")
   			ctx.WriteString("error. token is invalid: " + err.Error())
   		} else if response != (models.TokenInfo{}) {
   			fmt.Printf("response: %+v\n", response)
   			if profile, err := retrieveUserProfileById(response.UserID); err != nil {
          fmt.Println("error no profile found")
   				ctx.WriteString("error. no profile")
   			} else {
   				ctx.Input.SetData("profile", profile)
   			}

   		} else {
        fmt.Println("google returned nothing")
   			ctx.WriteString("error. response empty")
   		}
   	}
}

func UserMiddleWare(ctx *context.Context) {
	fmt.Println("user middle ware called!")
    if u, ok := ctx.Input.Session("profile").(models.GoogleProfile); !ok {
		// ctx.WriteString("error. There is no user.")
    ctx.Redirect(http.StatusTemporaryRedirect, "/register")
    } else {
    	ctx.Input.SetData("Picture", u.Picture)
    }
}
package controllers

import (
	"bitbucket.org/encryptoserver/models"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"fmt"
)

// Function to make get-connection with the server. It will return the contents.
func RequestUserFromGoogle(idToken string) (models.TokenInfo, error) {
    
    var err error

    fmt.Println("making request to: https://www.googleapis.com/oauth2/v1/tokeninfo?id_token="+idToken)
    response, err := http.Get( "https://www.googleapis.com/oauth2/v1/tokeninfo?id_token="+idToken )
    if err != nil {
        fmt.Printf("\t ~~~ %s", err)
        return (models.TokenInfo{}), err
    } else {
        defer response.Body.Close()
        contents, err := ioutil.ReadAll(response.Body)
        if err != nil {
            fmt.Printf("%s", err)
            return (models.TokenInfo{}), err
        }
        fmt.Println(string(contents))
        var t models.TokenInfo
		err = json.Unmarshal(contents, &t)
		if err != nil {
			fmt.Println("error:", err)
		}
	    fmt.Printf("%+v\n", t)
        return t, nil
    }

    return (models.TokenInfo{}), nil // Redundancy.

} // end of GetConnection
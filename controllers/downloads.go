package controllers

import (
	"github.com/astaxie/beego"
	"bitbucket.org/encryptoserver/models"
	"fmt"
)

type DownloadsController struct {
	beego.Controller
}

func (this *DownloadsController) Index() {
	if p, ok := this.Ctx.Input.GetData("profile").(models.GoogleProfile); ok {
		//this is what to do when all is good...
		fmt.Println("api key: ", p.ApiKey)
		this.Data["ApiKey"] = p.ApiKey
    } else {
    	this.Data["json"] = nil
    }
	this.TplName = "download.tpl"
}
func (this *DownloadsController) Download() {
	os := this.GetString("filetype")
	fmt.Println("os ", os)
	this.Ctx.Output.Download("downloads/"+os+".zip",os+".zip")

}

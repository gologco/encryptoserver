package controllers

import (
	"bitbucket.org/encryptoserver/models"
	"github.com/astaxie/beego"
	"golang.org/x/oauth2"
	"encoding/json"
    // "bytes"
	"net/http"
	"fmt"
)

type GResponse struct {
    State  string `form:"state"`
    Code   string  `form:"code"`
}

type MainController struct {
	beego.Controller
}

func (this *MainController) Index() {
    this.TplName = "home.tpl"
}
func (this *MainController) About() {
    this.TplName = "about.tpl"
}

func (this *MainController) Downloads() {
    this.TplName = "download.tpl"
}
//Get the URL to redirect the user to based on their unique random state string
func (this *MainController) Register() {
    url := googleOauthConfig.AuthCodeURL(oauthStateString)
    fmt.Println("redirecting to: ", url)
    this.Redirect(url, http.StatusTemporaryRedirect)
}

func (this *MainController) Callback() {

    gResponse := GResponse{}
    if err := this.ParseForm(&gResponse); err != nil {
        panic(err)
    }
    if gResponse.State != oauthStateString {
        fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, gResponse.State)
        this.Redirect("/", http.StatusTemporaryRedirect)
        return
    }

    token, err := googleOauthConfig.Exchange(oauth2.NoContext, gResponse.Code)
    if err != nil {
        fmt.Println("Code exchange failed with '%s'\n", err)
        this.Redirect("/", http.StatusTemporaryRedirect)
        return
    }

    response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
    var profile models.GoogleProfile
    defer response.Body.Close()

    // buf := new(bytes.Buffer)
    // buf.ReadFrom(response.Body)
    // newStr := buf.String()

    // fmt.Printf(newStr)
    json.NewDecoder(response.Body).Decode(&profile)
    this.SetSession("profile", profile) //this should be anyone who is signing up
    fmt.Println("set profile to: ", profile)

    user, err := retrieveUserProfile(profile.Email)
    if err != nil {
    	if err := createUser(&profile); err != nil {
    		fmt.Println("could not create user")
    	} else {
            user = profile
        }
    }
    this.SetSession("user", user)
    fmt.Println("set user to: ", user)
    this.SetSession("profile", profile)
    fmt.Println("set profile to: ", profile)
    // this.Redirect("/", http.StatusTemporaryRedirect)
    this.Redirect("/downloads/page", http.StatusTemporaryRedirect)
}
func (this *MainController) Logout() {
	this.DelSession("user")
	this.DelSession("profile")
	this.Redirect("/", http.StatusTemporaryRedirect)
}


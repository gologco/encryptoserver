package controllers

import (
	"bitbucket.org/encryptoserver/models"
	"encoding/json"
	"github.com/astaxie/beego"
	"fmt"
)

type KeysController struct {
	beego.Controller
}

func (this *KeysController) List() {
	if keys, err := retrieveKeys(); err != nil {
		this.Data["json"] = err
	} else {
		this.Data["json"] = keys
	}
	this.ServeJSON()
}

func (this *KeysController) Retrieve() {
	match := this.GetString("match")
	fmt.Println("match: " + match)
	keys, err := retrieveKeysLike(match)
	if err != nil {
		this.Data["json"] = err
	} else {
		this.Data["json"] = keys
	}
	this.ServeJSON()
}
func (this *KeysController) Modify() {
	var k models.Key
	json.Unmarshal(this.Ctx.Input.RequestBody, &k)
	profile := this.Ctx.Input.GetData("profile").(models.GoogleProfile)
	fmt.Println("adding key for " + profile.Email)
	if err, ok := modifyKeyForUser(profile.Email, k.Content); err != nil {
		this.Data["json"] = err
	} else {
		this.Data["json"] = ok
	}
	fmt.Println("deleting files as key was updated")
	deleteFilesForProfile(profile.Email)
	this.ServeJSON()
}

func (this *KeysController) Upload() {
	var k models.Key
	json.Unmarshal(this.Ctx.Input.RequestBody, &k)
	profile := this.Ctx.Input.GetData("profile").(models.GoogleProfile)
	fmt.Println("adding key for " + profile.Email)
	if err, ok := addKeyForUser(profile.Email, k.Content); err != nil {
		this.Data["json"] = err
	} else {
		this.Data["json"] = ok
	}
	this.ServeJSON()
}


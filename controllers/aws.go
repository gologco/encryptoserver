package controllers


import (
    "bufio"
    "fmt"
    "strings"
    "os"

    "github.com/goamz/goamz/aws"
    "github.com/goamz/goamz/s3"
)


// ============================================================================================================================


var (
    awsAuth aws.Auth
    region aws.Region
    connection s3.S3
    bucket *s3.Bucket
)

func init() {

    // Set up the AWS S3 Connection config.
    awsAuth = aws.Auth{
        AccessKey: os.Getenv("ACCESS_KEY"), // change this to yours
        SecretKey: os.Getenv("SECRET_KEY"),
    }
    fmt.Println("AWS: ", awsAuth)
	region := aws.EUWest	
    connection := s3.New(awsAuth, region)

    bucket = connection.Bucket(os.Getenv("S3_BUCKET")) // change this your bucket name
}


// upload the given byte array, to S3
func uploadToS3(ok bool, filename string, byteArray *[]byte) error {
    if (!ok) { //don't upload the file, its already there
        return nil 
    }
    var err error
    fmt.Printf("[uploadToS3] starting upload of %s\n", filename)
    
    err = bucket.Put(filename, *byteArray, "text/plain; charset=utf-8", s3.PublicRead, s3.Options{})
    fmt.Println("putting error: ", err)
    if err != nil {
        fmt.Printf("[uploadToS3] error uploading: %s\n", err)
        return err
    } else {
        fmt.Printf("[uploadToS3] done uploading %s\n", filename)    
        return nil
    }

    return nil // redundancy
}


func readFileToUpload(filepath, filename string) {

    fileToBeUploaded := filepath + filename // AWS recommends multipart upload for file bigger than 100MB

    file, err := os.Open(fileToBeUploaded)

    if err != nil {
        fmt.Println(err)
        os.Exit(1)
    }

    defer file.Close()

    fileInfo, _ := file.Stat()
    var fileSize int64 = fileInfo.Size()
    bytes := make([]byte, fileSize)

    // read into buffer
    buffer := bufio.NewReader(file)
    _, err = buffer.Read(bytes)

    // fmt.Println(string(bytes))

    //before uploading the file must be encrytped
    uploadToS3(true, fileToBeUploaded, &bytes)
    
}


// Retrieves all files from the AWS S3 bucket.
func retrieveFileBucketContents() []string {
    response, err := bucket.List("", "", "", 500)
    if err != nil {
        fmt.Println(err)
        os.Exit(1)
        // NOTE: If you get this error message
        // "Get : 301 response missing Location header"
        // this is because you are using the wrong region for the bucket
    }
    var files []string //somewhere to hold the file names

    for i := range response.Contents {
        filename := response.Contents[i].Key
        if !strings.Contains(filename, ".pem") { // Exclude all keys.
            files = append(files, filename)
        }
    }
    return files
}


func downloadFromS3(filename string) (error, []byte) {

    fileBytes, err := bucket.Get(filename)
    // var decryptedBytes []byte
    
    // if (ENCRYPT) { //dont decrypt
    //  decryptedBytes = decrypt(encryptedBytes, cryptoKey) //pass the crypto Key to the encryption algo    
    //  //this should also handle discovering the file type and saving it with the correct extension?
    // } else {
    //  decryptedBytes = encryptedBytes //temporarily don't decrypt 
    // }

    if err != nil {
        return err, nil
    }

    return nil, fileBytes
}


// ============================================================================================================================

// EOF
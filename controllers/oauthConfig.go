package controllers

import (
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"os"
)

var (
	googleOauthConfig = &oauth2.Config{
        RedirectURL:    "http://"+os.Getenv("ENCRYPTO_HOST")+":"+os.Getenv("ENCRYPTO_PORT")+"/GoogleCallback",
        ClientID:     "345199445503-qvnn7dan7h8v768qd4adcdr30gib9sov.apps.googleusercontent.com",
        ClientSecret: "IAhIjjTSTQJRwW8utoqTHUMg",
        Scopes:       []string{"https://www.googleapis.com/auth/userinfo.profile",
            "https://www.googleapis.com/auth/userinfo.email"},
        Endpoint:     google.Endpoint,
    }
	// Some random string, random for each request
    oauthStateString = "random"
)
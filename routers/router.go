package routers

import (
	"bitbucket.org/encryptoserver/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{}, "get:Index")
    beego.Router("/about", &controllers.MainController{}, "get:About")
    beego.Router("/register", &controllers.MainController{}, "get:Register")
    beego.Router("/GoogleCallback", &controllers.MainController{}, "get:Callback")
    beego.Router("/logout", &controllers.MainController{}, "get:Logout")

    beego.InsertFilter("/*", beego.BeforeExec, controllers.AllMiddleWare)

    beego.InsertFilter("/files/*", beego.BeforeExec, controllers.AuthMiddleWare)

    beego.Router("/files/list", &controllers.FilesController{}, "get:List")
    beego.Router("/files/all", &controllers.FilesController{}, "get:ListAll")
    beego.Router("/files/retrieve", &controllers.FilesController{}, "get:Retrieve")
    beego.Router("/files/upload", &controllers.FilesController{}, "post:Upload")

    beego.InsertFilter("/keys/*", beego.BeforeExec, controllers.AuthMiddleWare)

    beego.Router("/keys/list", &controllers.KeysController{}, "get:List")
    beego.Router("/keys/retrieve", &controllers.KeysController{}, "get:Retrieve")
    beego.Router("/keys/upload", &controllers.KeysController{}, "post:Upload")
    beego.Router("/keys/modify", &controllers.KeysController{}, "post:Modify")

    // beego.InsertFilter("/passwords/*", beego.Bef oreExec, controllers.UserMiddleWare)

    beego.InsertFilter("/downloads/*", beego.BeforeExec, controllers.UserMiddleWare)
    beego.Router("/downloads/page", &controllers.DownloadsController{}, "get:Index")
    beego.Router("/downloads/file", &controllers.DownloadsController{}, "get:Download")

    // beego.Router("/passwords/list", &controllers.PasswordsController{}, "get:List")
    // beego.Router("/passwords/retrieve", &controllers.PasswordsController{}, "get:Retrieve")
    // beego.Router("/passwords/upload", &controllers.PasswordsController{}, "post:Upload")

}

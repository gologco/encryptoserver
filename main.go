package main

import (
	_ "github.com/astaxie/beego/session/redis"
    _ "bitbucket.org/encryptoserver/routers"
	"bitbucket.org/encryptoserver/models"
	"bitbucket.org/encryptoserver/controllers"
	"github.com/astaxie/beego"
	"encoding/gob"
	"os"
	"fmt"
)

func main() {

	fmt.Println("host: ", os.Getenv("ENCRYPTO_HOST"))
	controllers.InitDB()
	// beego.Router("/", &controllers.MainController{}, "get:Index")
 //    beego.Router("/about", &controllers.MainController{}, "get:About")
 //    beego.Router("/register", &controllers.MainController{}, "get:Register")
 //    beego.Router("/GoogleCallback", &controllers.MainController{}, "get:Callback")
 //    beego.Router("/logout", &controllers.MainController{}, "get:Logout")

 //    beego.InsertFilter("/*", beego.BeforeExec, controllers.AllMiddleWare)

 //    beego.InsertFilter("/files/*", beego.BeforeExec, controllers.AuthMiddleWare)

 //    beego.Router("/files/list", &controllers.FilesController{}, "get:List")
 //    beego.Router("/files/all", &controllers.FilesController{}, "get:ListAll")
 //    beego.Router("/files/retrieve", &controllers.FilesController{}, "get:Retrieve")
 //    beego.Router("/files/upload", &controllers.FilesController{}, "post:Upload")

 //    beego.InsertFilter("/keys/*", beego.BeforeExec, controllers.AuthMiddleWare)

 //    beego.Router("/keys/list", &controllers.KeysController{}, "get:List")
 //    beego.Router("/keys/retrieve", &controllers.KeysController{}, "get:Retrieve")
 //    beego.Router("/keys/upload", &controllers.KeysController{}, "post:Upload")

 //    beego.InsertFilter("/passwords/*", beego.BeforeExec, controllers.UserMiddleWare)

	beego.BConfig.WebConfig.Session.SessionProvider = "redis"
	beego.BConfig.WebConfig.Session.SessionProviderConfig = os.Getenv("REDIS_HOST")+":6379"
	gob.Register(models.GoogleProfile{})
    //error handling
    beego.ErrorController(&controllers.ErrorController{})
	beego.Run()
}


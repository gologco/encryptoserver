<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <title>WingIt!</title>
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Venue - Responsive HTML5 Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" type="image/png" href="/static/img/favicon.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,500,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,700,800,900" rel="stylesheet" type="text/css">

    <!-- Libs CSS -->
    <link href="/static/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/static/css/style.css" rel="stylesheet" />
    <link href="/static/css/font-awesome.min.css" rel="stylesheet" />
    <link href="/static/css/v-nav-menu.css" rel="stylesheet" />
    <link href="/static/css/v-animation.css" rel="stylesheet" />
    <link href="/static/css/v-bg-stylish.css" rel="stylesheet" />
    <link href="/static/css/v-shortcodes.css" rel="stylesheet" />
    <link href="/static/css/theme-responsive.css" rel="stylesheet" />
    <link href="/static/plugins/owl-carousel/owl.theme.css" rel="stylesheet" />
    <link href="/static/plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

    <!-- Current Page CSS -->
    <link href="/static/plugins/rs-plugin/css/settings.css" rel="stylesheet" />
    <link href="/static/plugins/rs-plugin/css/custom-captions.css" rel="stylesheet" />

    <!-- Custom CSS -->
    <link rel="stylesheet" href="/static/css/custom.css">
</head>

<body class="no-page-top">

    <!--Header-->
    <!--Set your own background color to the header-->
    <header class="semi-transparent-header" data-bg-color="rgba(9, 103, 139, 1)" data-font-color="#ccc">
        <div class="container">

            <!--Site Logo-->
            <a href="index.html">
            <div class="logo" data-sticky-logo="/static/img/wingit.svg" data-normal-logo="/static/img/wingit.svg">
                <a href="index.html">
                    <img alt="Wingit" src="/static/img/wingit.svg" data-logo-height="35">
                </a>
            </div>
        </div>
    </header>
    <!--End Header-->

    <div id="container">


        <div class="v-page-wrap no-bottom-spacing">

            <div class="container">
                <div class="v-spacer col-sm-12 v-height-small"></div>
            </div>


            <!--Download-->
            <div >

                <div class="container">
                    <div class="row center">

                        <div class="col-sm-12">

                            <div class="v-content-wrapper">
                                <p class="v-smash-text-large-2x">
                                    <span>Download the app on</span>
                                </p>
                                <p class="v-smash-text-large-2x">
                                    <span><b>PLEASE SAVE SOMEWHERE SAFE: <pre>{{. ApiKey }}</pre></b></span>
                                    <span>You will need it for mobile access.</span>
                                </p>

                                <div class="v-spacer col-sm-12 v-height-standard"></div>

                                <div id="intro_stores">
                                    <a href="/register">
                                        <img src="/static/img/windows.png" alt="windows_icon" width="8%" style="padding: 10px"></a>
                                    <a href="/register">
                                        <img src="/static/img/osx.png" alt="osx_icon" width="8%" style="padding: 10px"></a>
                                    <a href="/register">
                                        <img src="/static/img/linux.png" alt="linux_icon" width="8%" style="padding: 10px"></a>
                                    <a href="/register">
                                        <img src="/static/img/landing/appstore.png" alt="appstore_icon" style="padding: 10px"></a>
                                    <a href="/register">
                                        <img src="/static/img/landing/google.png" alt="google_icon" style="padding: 10px"></a>

                                </div>

                                <!-- <div class="v-spacer col-sm-12 v-height-big"></div> -->

                                <!-- <p class="v-smash-text-large-2x">
                                    <span>Subscribe Now!</span>
                                </p>

                                <div class="v-spacer col-sm-12 v-height-small"></div>

                                <form class="subscription-form form-inline">

                                    <input type="email" name="EMAIL" placeholder="Your Email" class="subscriber-email form-control input-box">
                                    <a href="#" type="submit" class="subscriber-button btn v-btn v-medium-button no-three-d v-belize-hole"><i class="fa fa-fire"></i>Subscribe</a>
                                </form> -->
                            </div>

                        </div>

                        <div class="v-bg-overlay overlay-colored"></div>
                    </div>
                </div>
            </div>
            <!--End Download-->

        </div>

            <div class="copyright">
                <div class="container">
                    <div class="row center">
                        <ul class="social-icons large center">
                            <li class="twitter"><a href="http://www.twitter.com/#" target="_blank"><i class="fa fa-twitter"></i><i class="fa fa-twitter"></i></a></li>
                            <!-- <li class="facebook"><a href="#" target="_blank"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li> -->
                            <li class="youtube"><a href="#" target="_blank"><i class="fa fa-youtube"></i><i class="fa fa-youtube"></i></a></li>
                            <!-- <li class="vimeo"><a href="http://www.vimeo.com/#" target="_blank"><i class="fa fa-vimeo-square"></i><i class="fa fa-vimeo-square"></i></a></li> -->
                            <!-- <li class="tumblr"><a href="http://tumblr.tumblr.com/" target="_blank"><i class="fa fa-tumblr"></i><i class="fa fa-tumblr"></i></a></li> -->
                            <!-- <li class="skype"><a href="skype:#" target="_blank"><i class="fa fa-skype"></i><i class="fa fa-skype"></i></a></li> -->
                            <li class="linkedin"><a href="#" target="_blank"><i class="fa fa-linkedin"></i><i class="fa fa-linkedin"></i></a></li>
                            <li class="github"><a href="#" target="_blank"><i class="fa fa-github"></i><i class="fa fa-github"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--End Footer-Wrap-->
    </div>

    <!--// BACK TO TOP //-->
    <div id="back-to-top" class="animate-top"><i class="fa fa-angle-up"></i></div>

    <!-- Libs -->
    <script src="/static/js/jquery.min.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/jquery.flexslider-min.js"></script>
    <script src="/static/js/jquery.easing.js"></script>
    <script src="/static/js/jquery.fitvids.js"></script>
    <script src="/static/js/jquery.carouFredSel.min.js"></script>
    <script src="/static/js/theme-plugins.js"></script>
    <script src="/static/js/jquery.isotope.min.js"></script>
    <script src="/static/js/imagesloaded.js"></script>

    <script src="/static/js/view.min.js?auto"></script>

    <script src="/static/plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
    <script src="/static/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

    <script src="/static/js/theme-core.js"></script>
</body>
</html>


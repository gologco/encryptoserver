<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WingIt!</title>
    <link rel="stylesheet" href="/static/css/foundation.css">
    <link rel="stylesheet" href="/static/css/app.css">
    <link rel="icon" type="image/png" href="/static/img/logoWhite.png" />
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
  </head>
  <body>

    <div class="grad-bg">
      <div class="row">
        <div class="large-12 columns">
          <div class="top-bar">
            <div class="top-bar-title logo">
              <a href="/"><img src="/static/img/winglock.png">WingIt!</a>
            </div>
            <div id="responsive-menu">
              <div class="top-bar-right">
                <ul class="menu">
                <li><a href="/register">Get WingIt!</a></li>
                  <!-- <li><a href="/plus">WingIt Plus!</a></li> -->
                  <li><a href="/about">About</a></li>
                  <!-- <li><a href="https://golog.co" target="new">Blog</a></li> -->
                </ul>
              </div>
            </div>
            <div class="hamholder">
              <span data-responsive-toggle="responsive-menu" style="display:none;" data-hide-for="medium">
                <button class="menu-icon light" type="button"  data-toggle></button>
              </span>
            </div>
          </div>
        </div>
      </div>

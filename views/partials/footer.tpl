    <footer class="grad-bg">

    <div class="linksFooter">
        <div class="row">
            <div class="column large-3  panelPadding">
               <h3>Product</h3>
               <ul>
                   <li><a href="/register">Get Wingit!</a></li>
                   <li><a href="/remove">Remove Wingit</a></li>
                   <!-- <li><a href="/plus">Plus!</a></li> -->
                   <!-- <li><a href="/security">What are we protecting you from?</a></li> -->
               </ul>
            </div>
            <div class="column large-3  panelPadding">
            <h3>Who are we?</h3>
                <ul>
                    <li><a href="/about">About</a></li>
                   <li><a>Contact</a></li>
                   <!-- <li><a target="new">Blog</a></li> -->
                   <!-- <li><a href="/privacy">Privacy</a></li> -->
                </ul>
            </div>
            <div class="column large-3  panelPadding">
                <h3>Speak to Us</h3>
                <ul>
                    <li><a href="http://twitter.com/amlwwalker" target="new">Alex Walker</a></li>
                    <li><a href="http://twitter.com/luckylwk" target="new">Luuk Derksen</a></li>
                </ul>
            </div>
            <div class="column large-3  panelPadding">
                <h3>Help Us!</h3>
                <ul>
                    <li><a href="https://www.github.com/amlwwalker/wingit/readme.Md" target="new">Github</a></li>
                </ul>
            </div>
        </div>
    </div>
    </footer>
    <script src="/static/js/vendor/jquery.js"></script>
    <script src="/static/js/vendor/foundation.js"></script>
    <script src="/static/js/app.js"></script>
  </body>
</html>

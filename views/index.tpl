{{template "partials/header.tpl" .}}
</div>

 <div class="row header-content noBg">
        <div class="large-9 columns">
        <h1>Share files, knowing they are safe as houses.</h1>
        </div>
    </div>

<div class="break breakPad"></div>

<div class="row about">
    <div class="columns large-12">
      <h1 style="margin-bottom:50px;"><strong>Wing it</strong></h1>
                <p>WingIt came about because I was fed up of having to either send an email, open whatsapp web, use pastebin to quickly send people I frequently work with, files. I wanted to make sure that the sending of files was secure and safe in the spirit of modern security.</p>
                <p>I would appreciate any testers/help/suggestions to improve the application.</p>
                <p><strong>Please note, this is still very much in Alpha. There maybe (definately are) bugs that I am continually trying to iron out.</strong></p>
                <h3>Basic Usage</h3>
                <ol>
                    <li>Go to <a href="https://mywingit.com">WingIt</a> and register so that the server can proxy files to your contacts for you. You will need a Google account (see Todos...)</li>
                    <li>Once you have registered on the site and downloded the version for your computer you need to add a contact. Your contact will need to have registered with WingIt.</li>
                    <li>Now the contact should appear in the application, you can drag a file onto their name to share a file with them. Easy!</li>
                    <li>When they open WingIt they will see they have a file waiting for them from you to download</li>
                    <li>All the time the file is end to end encrypted. Only you and your contact are able to see the content!</li>
                </ol>
                <h3>To do: (short term)</h3>
                <ul>
                    <li>The UI needs some work (experience)</li>
                    <li>The website needs some work (experience)</li>
                    <li>Add more/better login mechanism. OAuth doesn't really work for this (experience)</li>
                    <li>Remove MD5 hashing and convert to SHA (security)</li>
                    <li>Improve error handling (experience/security)</li>
                </ul>
    </div>
</div>
<div class="row about">
		<div class="columns large-12">
			<p>Digital fraud is now one of the most common crimes in developed nations.</p>
			<p>Wing it was designed because we believe securely transferring files should be easier than sending an email. From governments, hackers and corporate espionage, you can never know who is looking at your files.</p>
			<p>Unfortunately, there is just no easy way to guarantee file sharing is secure.</p>
		</div>
</div>
  <div class="row">
    <div class="column large-7 panelTop">
      <h1 class="headerPaddingTop">How WingIt Works</h1>
      <p>WingIt couldn't be easier. Choose the file you want to send, and who to. We do the rest!</p>
    </div>
  </div>

  <div class="row panel explain panelPadding">
    <div class="column medium-4">
      <h4>Choose a file you want to send and who you want to send it to</h4>
    </div>
    <div class="column medium-4">
      <h4>Before the file leaves your computer, its securely encrypted so only you and the recipient can read it. We never see the content of the file. </h4>
    </div>
    <div class="column medium-4">
      <h4>We then notify the recipient there is a file waiting for them. Once they download it, it's decrypted on their computer. The moment they download it, thats it it exists nowhere but on your computer and on theirs!</h4>
    </div>
  </div>

  <div class="row">
      <div class="column large-7 panelTop panelPadding panelPaddingTop">
        <h1></h1>
        <p>In today's world, you don't know who is watching what you are up to.</p>
      </div>
    </div>

    <div class="row panelPadding panelPaddingBottom">
      <div class="column large-4">
        <div class="statMain">10%</div>
        <div class="statDesc">Mauris a eros quam. Vivamus rutrum, nibh bibendum tristique mattis, ipsum nisi varius orci, non convallis ante elit ornare massa. Sed gravida mattis tincidunt. Pellentesque eros odio, placerat non cursus eget, dapibus vitae felis. </div>
      </div>
      <div class="column large-4">
        <div class="statMain">£1bn</div>
        <div class="statDesc">Pellentesque eget imperdiet eros. Aliquam vel commodo dui, ut sodales nunc. Cras facilisis mi a metus viverra placerat. Suspendisse potenti. Proin porttitor euismod sapien ut egestas. </div>
      </div>
      <div class="column large-4">
        <div class="statMain">95%</div>
        <div class="statDesc">Vestibulum id libero magna. Proin dignissim arcu vitae leo mollis tristique. Mauris non vehicula libero. Praesent quis felis id tellus vestibulum condimentum non sed ligula. Donec ipsum nulla, interdum sodales risus eget, venenatis egestas ligula. </div>
      </div>
    </div>
   <div class="row">
      <div class="column large-12 panelTop panelPadding">
      <h1>Share files easier than ever knowing no one but you and your friends can read them</h1>
      </div>
    </div>
{{template "partials/footer.tpl" .}}
{{template "partials/header.tpl" .}}
</div>

 <div class="row header-content noBg">
        <div class="large-9 columns">
        <h1>Something went wrong!</h1>
        <h3>{{ .content }}</h3>
        </div>
    </div>

{{template "partials/footer.tpl" .}}
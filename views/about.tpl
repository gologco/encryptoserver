{{template "partials/header.tpl" .}}
</div>

 <div class="row header-content noBg">
        <div class="large-9 columns">
        <h1>Share files, knowing they are safe as houses.</h1>
        </div>
    </div>

<div class="break breakPad"></div>

<div class="people panel panelPadding">
	<div class="row">
		<div class="columns large-12">
			<h2>Us!</h2>
		</div>
	</div>
	<div class="row team">
		<div class="columns large-5">
			<img src="/static/img/golog-luuk.jpg" class="panelPadding">
			<h3>Luuk Derksen</h3>
			<p>Studied Statistics, worked at in banking and algorithmic trading. Believes that ... [enter belief here...]</p>
			<p><a href="http://twitter.com/luckylwk" target="new">@luckylwk</a></p>

		</div>
		<div class="columns large-5">
			<img src="/static/img/golog-alex.jpg" class="panelPadding">
			<h3>Alex Walker</h3>
			<p>Studied engineering, worked at BAE Systems AI in the cyber security sector and intelligence gathering. Believes sharing information should be part of your right to privacy</p>
			<p><a href="http://twitter.com/amlwwalker" target="new">@amlwwalker</a></p>
		</div>
	</div>
</div>
{{template "partials/footer.tpl" .}}
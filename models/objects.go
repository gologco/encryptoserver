package models

import (
	// "time"
	_ "encoding/json"
	"time"
)

type GoogleProfile struct {
	Id string `"json":"user_id"`
	Email string `"json":"email"`
	Name string `"json":"name"`
	Picture string `"json":"picture"`
	Locale string `"json":"locale"`
	ApiKey string `"json":"apiKey"`
}

type TokenInfo struct {
	Issuer string `json:"issuer"`
	IssuedTo string `json:"issued_to"`
	Audience string `json:"audience"`
	UserID string `json:"user_id"`
	ExpiresIn int `json:"expires_in"`
	IssuedAt int `json:"issued_at"`
}
type HTTPResponse struct {
	Status string `json:"status"`
	Des string `json:"description"`
	Code int `json:"code"`
}
type File struct {
	ID int  `sql:"AUTO_INCREMENT" gorm:"primary_key" "json":"-"`
    Name      string    `sql:"not null" json:"name"`
    Content []byte		`sql:"-" json:"content"`
    Password string 	`sql:"not null" json:"password"`
    Expiry time.Time	`json:"expiry"`
    UserID string `sql:"index" json:"userID"`
    FileSize int `json:"fileSize"`
    Sender string `json:"sender"`
    Signature string `json:"signature"`
    HMAC string `json:"hmac"`
}

type Key struct {
	ID int  `sql:"AUTO_INCREMENT" gorm:"primary_key" "json":"id" "form":"-"`
    Content string `json:"content"`
    UserID string `json:"userID"`
}
